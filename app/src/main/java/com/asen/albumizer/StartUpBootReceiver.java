package com.asen.albumizer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.asen.albumizer.Persistence.Storage;

public class StartUpBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Storage storage = new Storage(context);
        if(storage.shouldMediaWatcherBeRun())
            MediaWatcherJob.scheduleJob(context);

    }
}
