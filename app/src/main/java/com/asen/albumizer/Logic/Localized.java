package com.asen.albumizer.Logic;

public interface Localized {

    String getLocalizedString(int id);
    String getLocalizedAndFormattedString(int id, Object... args);

}
