package com.asen.albumizer.Logic;

public interface Toastable {

    void showToast(int stringId);
    void showToast(String message);

}
