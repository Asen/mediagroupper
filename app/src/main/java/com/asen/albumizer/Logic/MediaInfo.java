package com.asen.albumizer.Logic;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.provider.DocumentFile;

import com.asen.albumizer.Errors.MediaNotFoundError;
import com.asen.albumizer.Persistence.Storage;

import java.util.Set;

public class MediaInfo
{

    private Uri mediaUri;
    private String path;
    private long takenTs;
    private int id;
    private String name;


    /**
     * @param contentResolver
     * @param uri
     * @throws MediaNotFoundError
     */
    public MediaInfo(ContentResolver contentResolver, Uri uri) throws MediaNotFoundError {

        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        if(cursor == null)
            throw new MediaNotFoundError();

        cursor.moveToFirst();

        if(cursor.getCount() == 1){
            mediaUri = uri;
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
            takenTs = cursor.getLong((cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATE_TAKEN))) / 1000;
            id = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID));
            name = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
        } else {
            throw new MediaNotFoundError();
        }

        cursor.close();
    }


    public boolean isExists()
    {
        return path != null;
    }


    public boolean isTakenRecently()
    {
        long now = System.currentTimeMillis() / 1000;
        return (now - takenTs) < 60;
    }


    @Nullable
    public int getMediaId()
    {
        return id;
    }


    @Nullable
    public String getPath() {
        return path;
    }


    @Nullable
    public String getName()
    {
        return name;
    }


    @Nullable
    public Uri getDocumentUri(Context context) throws MediaNotFoundError
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            try{
                return MediaStore.getDocumentUri(context, mediaUri);
            } catch (IllegalStateException e){
                throw new MediaNotFoundError();
            }

        } else {

            //content://com.android.externalstorage.documents/tree/B68A-3366:DCIM/document/B68A-3366:DCIM/Camera/20180825_192019.jpg
            //content://com.android.externalstorage.documents/tree/B68A-3366%3ADCIM/document/B68A-3366%3ADCIM%2FCamera%2F20180825_192019.jpg

            MediaInfo mediaInfo = new MediaInfo(context.getContentResolver(), mediaUri);
            String path = mediaInfo.getPath().split("DCIM/")[1];
            String[] pathParts = path.trim().split("/");

            Set<Uri> uris = new Storage(context).getDcimUriInfo();
            for (Uri uri : uris) {
                DocumentFile df = DocumentFile.fromTreeUri(context, uri);
                for (String pathPart : pathParts)
                    df = df == null ? null : df.findFile(pathPart);

                if (df != null)
                    return df.getUri();
            }

            throw new MediaNotFoundError();
        }
    }

}
