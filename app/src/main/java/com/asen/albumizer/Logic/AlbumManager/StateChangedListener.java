package com.asen.albumizer.Logic.AlbumManager;

public interface StateChangedListener {

    void onStateChanged();

}
