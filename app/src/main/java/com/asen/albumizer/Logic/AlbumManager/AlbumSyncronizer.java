package com.asen.albumizer.Logic.AlbumManager;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.support.v4.provider.DocumentFile;

import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.DB.AlbumDAO;
import com.asen.albumizer.Persistence.DB.DB;
import com.asen.albumizer.Persistence.FS.AlbumFS;

import java.util.ArrayList;
import java.util.List;

public class AlbumSyncronizer
{

    private StateChangedListener listener;
    private boolean albumsDbUpdated = false;


    public AlbumSyncronizer(StateChangedListener listener) {
        this.listener = listener;
    }


    public void synchronizeAlbumsWithFS(Context context, ContentResolver resolver)
    {
        albumsDbUpdated = false;
        AlbumDAO dao = DB.getDatabase(context).albumDAO();
        AlbumFS albumFs = new AlbumFS(context, resolver);
        synchronizeWithFS(context, dao, albumFs);
        albumFs.setKnownAlbums(dao.getAlbums());

        if(albumsDbUpdated && listener!=null)
            listener.onStateChanged();
    }


    private void synchronizeWithFS(Context context, AlbumDAO dao, AlbumFS fsSource)
    {
        List<Album> dbAlbums = dao.getAlbums();

        // update via FS missing albums
        for(Album dbAlbum : dbAlbums){
            DocumentFile albumDir = DocumentFile.fromTreeUri(context, dbAlbum.getPathAsUri()).findFile(dbAlbum.getName());
            if(albumDir == null || !albumDir.exists()) {
                dao.delete(dbAlbum);
                albumsDbUpdated = true;
            }
        }

        ArrayList<Album> fsAlbums = fsSource.getSavedAlbums();

        for(Album fsAlbum : fsAlbums){

            boolean existsInDb = dbAlbums.stream().
                    anyMatch(dbAlbum -> fsAlbum.getName().equalsIgnoreCase(dbAlbum.getName()));

            if(!existsInDb){
                Uri dcimRoot = fsAlbum.getPathAsUri();
                DocumentFile dcimRootFolder = DocumentFile.fromTreeUri(context, dcimRoot);
                if(dcimRootFolder != null && dcimRootFolder.exists()){
                    DocumentFile fsAlbumFile = dcimRootFolder.findFile(fsAlbum.getName());
                    if(fsAlbumFile != null && fsAlbumFile.exists()) {
                        Album missingAlbum = new Album(fsAlbum.getName(), dcimRoot.toString(), fsAlbum.hidden);
                        dao.insert(missingAlbum);
                        albumsDbUpdated = true;
                    }
                }
            }
        }
    }

}
