package com.asen.albumizer.Logic.AlbumManager;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.provider.DocumentFile;

import com.asen.albumizer.Errors.AlbumAlreadyExistsError;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.DB.DB;

import java.io.FileNotFoundException;

public class AlbumManager
{

    @WorkerThread
    public void synchronizeAlbumsWithFS(Context context, ContentResolver resolver, StateChangedListener listener)
    {
        new AlbumSyncronizer(listener).synchronizeAlbumsWithFS(context, resolver);
    }


    @WorkerThread
    public void deleteAlbumWithFullSync(Context context, ContentResolver resolver, @NonNull Album album)
    {
        if (album.id < 0)
            throw new AssertionError();
        if(album.getPathAsUri() == null)
            throw new AssertionError();

        DB.getDatabase(context).albumDAO().delete(album);

        try {
            DocumentFile albumFile = DocumentFile.fromTreeUri(context, album.getPathAsUri()).findFile(album.getName());
            DocumentsContract.deleteDocument(resolver, albumFile.getUri());
            synchronizeAlbumsWithFS(context, resolver, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    @WorkerThread
    public void renameAlbumWithFullSync(Context context, ContentResolver resolver, int albumId, String newName) throws AlbumAlreadyExistsError {
        Album album = DB.getDatabase(context).albumDAO().getById(albumId);
        DocumentFile albumRoot = DocumentFile.fromTreeUri(context, album.getPathAsUri());
        DocumentFile albumFile = albumRoot.findFile(album.getName());

        DocumentFile possibleExisitingFile = albumRoot.findFile(newName);
        if(possibleExisitingFile!=null && possibleExisitingFile.exists())
            throw new AlbumAlreadyExistsError();

        DB.getDatabase(context).albumDAO().rename(albumId, newName);

        try {
            DocumentsContract.renameDocument(resolver, albumFile.getUri(), newName);
            synchronizeAlbumsWithFS(context, resolver, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
