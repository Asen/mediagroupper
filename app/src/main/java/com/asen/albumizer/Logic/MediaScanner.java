package com.asen.albumizer.Logic;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.asen.albumizer.Logging.TerminalLogger;

import java.util.ArrayList;
import java.util.List;

public class MediaScanner
{

    public List<Uri> getNewestMedia(ContentResolver resolver)
    {
        List<Uri> allChangedUris = scanRecentMedia(resolver, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        List<Uri> internalChanges = scanRecentMedia(resolver, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        allChangedUris.addAll(internalChanges);
        return allChangedUris;
    }


    private List<Uri> scanRecentMedia(ContentResolver resolver, Uri baseContentUri)
    {
        List<Uri> uris = new ArrayList<>();
        long duringLast15Minutes = System.currentTimeMillis() - 60000 * 15;

        Cursor cursor = resolver.query(
                baseContentUri,
                new String[]{ MediaStore.Images.ImageColumns._ID },
                MediaStore.Images.ImageColumns.DATE_TAKEN + " > ?",
                new String[] { String.valueOf(duringLast15Minutes) },
                null);

        if(cursor == null){
            new TerminalLogger().logError("Cursor is NULL: " + baseContentUri);
            return uris;
        }

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID));
                Uri uri = Uri.withAppendedPath(baseContentUri, String.valueOf(id));
                uris.add(uri);
                cursor.moveToNext();
            }
        }

        cursor.close();

        return uris;
    }

}
