package com.asen.albumizer.Logic;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.media.ExifInterface;
import android.support.v4.provider.DocumentFile;

import java.io.IOException;
import java.io.InputStream;

public class BitmapConverter
{

    private ContentResolver resolver;


    public BitmapConverter(ContentResolver resolver)
    {
        this.resolver = resolver;
    }


    public Bitmap convertToBitmap(DocumentFile img, int requestedWidth) throws IOException
    {
        InputStream resolverIs = resolver.openInputStream(img.getUri());
        if(resolverIs == null){
            return Bitmap.createBitmap(50,50, Bitmap.Config.ALPHA_8);
        }

        ExifInterface exif = new ExifInterface(resolverIs);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
        Matrix matrix = new Matrix();

        switch (orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                break;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(resolver.openInputStream(img.getUri()), null, options);

        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateInSampleSize(options, requestedWidth,0);

        Bitmap initialBmp = (BitmapFactory.decodeStream(resolver.openInputStream(img.getUri()), null, options));
        initialBmp = Bitmap.createBitmap(initialBmp, 0, 0, initialBmp.getWidth(), initialBmp.getHeight(), matrix, false);

        int scaledBmpHeight = (int)(((float)initialBmp.getHeight()/initialBmp.getWidth()) * requestedWidth);
        return Bitmap.createScaledBitmap(initialBmp, requestedWidth, scaledBmpHeight, false);
    }


    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth)
                inSampleSize *= 2;

        }

        return inSampleSize;
    }

}
