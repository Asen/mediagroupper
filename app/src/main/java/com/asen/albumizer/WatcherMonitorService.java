package com.asen.albumizer;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.NonNull;

import com.asen.albumizer.Logging.Logger;
import com.asen.albumizer.Logging.TerminalLogger;
import com.asen.albumizer.Persistence.Storage;

import org.acra.ACRA;

import java.util.List;

public class WatcherMonitorService extends JobService
{

    private static final int JOB_ID = 2;
    private Logger logger = new TerminalLogger();


    public static void start(@NonNull Context context)
    {
        if(isScheduled(context))
            stop(context);

        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID,
                new ComponentName(BuildConfig.APPLICATION_ID, WatcherMonitorService.class.getName()));
        builder.setPeriodic(1000 * 5 * 60);
        builder.setPersisted(true);

        JobScheduler js = context.getSystemService(JobScheduler.class);
        js.schedule(builder.build());

        new TerminalLogger().logInfo("WatcherMonitor: start");
    }


    public static void stop(@NonNull Context context)
    {
        JobScheduler js = context.getSystemService(JobScheduler.class);
        js.cancel(JOB_ID);
    }


    public static boolean isScheduled(Context context) {
        JobScheduler js = context.getSystemService(JobScheduler.class);
        List<JobInfo> jobs = js.getAllPendingJobs();
        if (jobs == null) {
            return false;
        }
        for (int i=0; i<jobs.size(); i++) {
            if (jobs.get(i).getId() == JOB_ID) {
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean onStartJob(JobParameters params)
    {
        logger.logInfo("WatcherMonitor: onStartJob");

        Context context = getApplicationContext();

        try {
            Storage storage = new Storage(context);
            if (storage.shouldMediaWatcherBeRun() && !MediaWatcherJob.isScheduled(context))
                MediaWatcherJob.scheduleJob(context);
        } catch (Throwable anyError){
            ACRA.getErrorReporter().handleException(anyError);
        }

        start(context);

        return false;
    }


    @Override
    public boolean onStopJob(JobParameters params)
    {
        logger.logInfo("WatcherMonitor: onStopJob");
        return false;
    }
}
