package com.asen.albumizer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.WorkerThread;
import android.support.v4.app.NotificationCompat;
import android.support.v4.provider.DocumentFile;

import com.asen.albumizer.Errors.MediaNotFoundError;
import com.asen.albumizer.Logging.Logger;
import com.asen.albumizer.Logging.TerminalLogger;
import com.asen.albumizer.Logic.MediaInfo;
import com.asen.albumizer.Logic.MediaScanner;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.DB.DB;
import com.asen.albumizer.Logic.Localized;
import com.asen.albumizer.UI.OnNewMediaActivity;

import org.acra.ACRA;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class MediaWatcherJob extends JobService implements Localized
{
    static final List<String> EXTERNAL_PATH_SEGMENTS = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.getPathSegments();
    static final JobInfo JOB_INFO;
    static final int JOB_ID = 1;

    final Handler mHandler = new Handler();
    final Runnable mWorker = new Runnable() {
        @Override public void run() {
            try {
                jobFinished(mRunningParams, false);
                scheduleJob(MediaWatcherJob.this);
            } catch (Throwable anyError){
                ACRA.getErrorReporter().handleException(anyError);
            }
        }
    };

    JobParameters mRunningParams;
    static Set<Integer> alertedIds = new HashSet<>();
    static int notificationCounter = 0;
    static Logger logger = new TerminalLogger();


    static {
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID,
                new ComponentName(BuildConfig.APPLICATION_ID, MediaWatcherJob.class.getName()));
        // Look for specific changes to images in the provider.
        builder.addTriggerContentUri(new JobInfo.TriggerContentUri(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                JobInfo.TriggerContentUri.FLAG_NOTIFY_FOR_DESCENDANTS));
        builder.addTriggerContentUri(new JobInfo.TriggerContentUri(
                MediaStore.Images.Media.INTERNAL_CONTENT_URI,
                JobInfo.TriggerContentUri.FLAG_NOTIFY_FOR_DESCENDANTS));
        // Also look for general reports of changes in the overall provider.
        builder.addTriggerContentUri(new JobInfo.TriggerContentUri(
                Uri.parse("content://" + MediaStore.AUTHORITY + "/"),
                0));
        builder.setTriggerContentUpdateDelay(TimeUnit.SECONDS.toMillis(1));
        builder.setTriggerContentMaxDelay(TimeUnit.SECONDS.toMillis(100));
        builder.setMinimumLatency(TimeUnit.SECONDS.toMillis(1));
        //builder.setOverrideDeadline(TimeUnit.SECONDS.toMillis(5));
        builder.setRequiresDeviceIdle(false);
        builder.setRequiresCharging(false);
        JOB_INFO = builder.build();
    }


    // Schedule this job, replace any existing one.
    public static int scheduleJob(Context context)
    {
        if(isScheduled(context))
            cancelJob(context);

        JobScheduler js = context.getSystemService(JobScheduler.class);
        if(js != null){
            int result = js.schedule(JOB_INFO);
            logger.logInfo(result == JobScheduler.RESULT_SUCCESS ? "Job scheduled" : "Job schedule failed!");
            if(result != JobScheduler.RESULT_SUCCESS)
                throw new RuntimeException("Could not schedule job");
            return result;
        } else {
            throw new RuntimeException("Could not obtain JoBScheduler service");
        }
    }


    // Check whether this job is currently scheduled.
    public static boolean isScheduled(Context context)
    {
        JobScheduler js = context.getSystemService(JobScheduler.class);
        if(js == null)
            return false;

        List<JobInfo> jobs = js.getAllPendingJobs();
        if (jobs.size() == 0)
            return false;

        for (int i=0; i<jobs.size(); i++) {
            if (jobs.get(i).getId() == JOB_ID)
                return true;
        }

        return false;
    }


    // Cancel this job, if currently scheduled.
    public static void cancelJob(Context context)
    {
        JobScheduler js = context.getSystemService(JobScheduler.class);
        if(js != null)
            js.cancel(JOB_ID);
    }


    @Override
    public boolean onStartJob(final JobParameters params)
    {
        logger.logInfo("Job onStart");

        mRunningParams = params;
        doInBackground(params, () -> mHandler.post(mWorker));

        return true;
    }


    private void doInBackground(final JobParameters params, Runnable onComplete)
    {
        Thread worker = new Thread(() -> {

            ArrayList<Uri> changedUris = new ArrayList<>();

            if (params.getTriggeredContentUris() != null && params.getTriggeredContentAuthorities() != null) {

                for (Uri uri : getNewUris()) {
                    List<String> path = uri.getPathSegments();
                    if (path != null && path.size() == EXTERNAL_PATH_SEGMENTS.size()+1) {

                        try{

                            MediaInfo mediaInfo = new MediaInfo(getContentResolver(), uri);
                            int mediaId = mediaInfo.getMediaId();

                            if(shouldAlert(mediaInfo)){
                                changedUris.add(uri);
                                alertedIds.add(mediaId);
                            }

                        } catch (MediaNotFoundError err){
                            logger.logError("Uri not found in watcher job: "+uri.toString());
                        }

                    }
                }
            }

            // alert only if changes not too much
            if(changedUris.size() <= 10) {
                for (final Uri changedUri : changedUris) {
                    onUriActuallyChanged(changedUri);
                }
            }

            onComplete.run();
        });

        worker.setUncaughtExceptionHandler((t, e) -> {
            ACRA.getErrorReporter().handleException(e);
            onComplete.run();
        });

        worker.start();
    }


    private List<Uri> getNewUris()
    {
        return new MediaScanner().getNewestMedia(getContentResolver());
    }


    private boolean shouldAlert(MediaInfo mediaInfo)
    {
        boolean isMediaWasNotAlerted =  !alertedIds.contains(mediaInfo.getMediaId());
        boolean isMediaFromAlbum = isMediaInKnownAlbum(mediaInfo);
        
        return mediaInfo.isExists() &&
                mediaInfo.isTakenRecently() &&
                isMediaWasNotAlerted &&
                !isMediaFromAlbum;
    }


    @WorkerThread
    private boolean isMediaInKnownAlbum(MediaInfo media)
    {
        List<Album> albums = DB.getDatabase(this).albumDAO().getAlbums();
        String mediaName = new File(media.getPath()).getName();

        for(Album album : albums){
            DocumentFile doc = DocumentFile.fromTreeUri(this, album.getPathAsUri()).findFile(album.getName());
            if(doc != null && doc.findFile(mediaName)!=null)
                return true;
        }

        return false;
    }


    private void onUriActuallyChanged(Uri changedUri)
    {
        int notificationId = notificationCounter++;

        Intent activityIntent =
                OnNewMediaActivity.getCreationIntent(this, changedUri, notificationId)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        showMediaDetectedNotification(activityIntent, notificationId, changedUri);
        startActivity(activityIntent);
    }


    private void showMediaDetectedNotification(Intent activityIntent, int notificationId, Uri imageUri)
    {
        String notificationChannelId = "AlbumizerChannel";
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), notificationChannelId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notificationId, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.on_new_photo);
        mBuilder.setContentTitle(getLocalizedString(R.string.notification_title));
        mBuilder.setContentText(getLocalizedString(R.string.notification_content));
        mBuilder.setPriority(Notification.PRIORITY_MAX);

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
            mBuilder.setLargeIcon(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if(mNotificationManager != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(
                        notificationChannelId,
                        getLocalizedString(R.string.notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                mNotificationManager.createNotificationChannel(channel);
            }

            mNotificationManager.notify(notificationId, mBuilder.build());
        }
    }


    @Override
    public boolean onStopJob(JobParameters params)
    {
        mHandler.removeCallbacks(mWorker);
        logger.logInfo("Job onStop");
        scheduleJob(MediaWatcherJob.this);
        return false;
    }


    @Override
    public String getLocalizedString(int id)
    {
        return getString(id);
    }


    @Override
    public String getLocalizedAndFormattedString(int id, Object... args)
    {
        return getString(id, args);
    }
}
