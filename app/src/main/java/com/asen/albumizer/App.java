package com.asen.albumizer;

import android.app.Application;
import android.content.Context;

import org.acra.ACRA;
import org.acra.annotation.AcraCore;
import org.acra.annotation.AcraMailSender;
import org.acra.annotation.AcraNotification;


@AcraCore(
        buildConfigClass = BuildConfig.class
)
@AcraMailSender(
        mailTo = "antochi.anton@ya.ru",
        resSubject = R.string.crash_subject,
        reportAsFile = true
)
@AcraNotification(
        resTitle = R.string.crash_notification_title,
        resText = R.string.crash_notification_text,
        resChannelName = R.string.crash_notification_channel_name
)
public class App extends Application {

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        ACRA.init(this);

        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            ACRA.getErrorReporter().handleException(e);
        });
    }
}
