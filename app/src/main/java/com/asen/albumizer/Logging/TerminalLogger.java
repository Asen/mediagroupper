package com.asen.albumizer.Logging;

import android.util.Log;

public final class TerminalLogger implements Logger
{

    private static final String LOG_TAG = "Albumizer_Logging";

    @Override
    public void logError(String errorMsg) {
        Log.e(LOG_TAG, errorMsg);
    }

    @Override
    public void logInfo(String errorMsg) {
        Log.i(LOG_TAG, errorMsg);
    }

}
