package com.asen.albumizer.Logging;

public interface Logger {

    void logError(String errorMsg);
    void logInfo(String errorMsg);

}
