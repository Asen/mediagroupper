package com.asen.albumizer.Persistence;

import android.content.Context;
import android.net.Uri;

import com.securepreferences.SecurePreferences;

import java.util.HashSet;
import java.util.Set;

public class Storage {


    private static SecurePreferences prefs;


    public Storage(Context context)
    {
        if(prefs == null)
            prefs = new SecurePreferences(context);
    }


    public void putServiceStateInfo(boolean isActive)
    {
        prefs.edit().putBoolean("service_state_run", isActive).apply();
    }


    public boolean shouldMediaWatcherBeRun()
    {
        return prefs.getBoolean("service_state_run", false);
    }


    public synchronized void putDcimUriInfo(Set<Uri> uris)
    {
        Set<String> stringifiedUris = new HashSet<>();
        for(Uri uri : uris)
            if(uri != null)
                stringifiedUris.add(uri.toString());

        prefs.edit().putStringSet("dcim_uris", stringifiedUris).commit();
    }


    public Set<Uri> getDcimUriInfo()
    {
        Set<Uri> uris = new HashSet<>();
        for(String stringUri : prefs.getStringSet("dcim_uris", new HashSet<String>()))
            if(stringUri != null)
                uris.add(Uri.parse(stringUri));

        return uris;
    }


    public Uri getPriorityDcimRoot()
    {
        Set<Uri> dcimRoots = getDcimUriInfo();

        for(Uri uri : dcimRoots)
            if(uri.toString().contains("Primary"))
                return uri;

        return (Uri) dcimRoots.toArray()[0];
    }


}
