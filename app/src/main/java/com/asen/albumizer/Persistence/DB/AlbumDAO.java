package com.asen.albumizer.Persistence.DB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface AlbumDAO
{

    @Query("SELECT * FROM albums")
    List<Album> getAlbums();


    @Query("SELECT * FROM albums WHERE id=:albumId")
    Album getById(int albumId);


    @Insert(onConflict = OnConflictStrategy.FAIL)
    long insert(Album director);


    @Delete
    int delete(Album album);


    @Query("UPDATE albums SET hidden=0 WHERE id=:albumId")
    int setAlbumVisible(int albumId);


    @Query("UPDATE albums SET hidden=1 WHERE id=:albumId")
    int setAlbumInvisible(int albumId);


    @Query("UPDATE albums SET album_name=:newName WHERE id=:albumId")
    void rename(int albumId, String newName);

}
