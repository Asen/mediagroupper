package com.asen.albumizer.Persistence.FS;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.support.v4.provider.DocumentFile;

import com.asen.albumizer.Errors.FsSyncResourceCreationError;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.Storage;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AlbumFS
{

    private static final String FS_RESOURCE = "album_inf";

    private ContentResolver resolver;
    private DocumentFile fsResourseFile;


    public  AlbumFS(Context context, ContentResolver resolver)
    {
        this.resolver = resolver;
        this.fsResourseFile = getFsResource(context);
    }


    private synchronized DocumentFile getFsResource(Context context)
    {
        Storage storage = new Storage(context);
        Set<Uri> dcimRoots = storage.getDcimUriInfo();

        for(Uri dcimRoot : dcimRoots){
            DocumentFile doc = DocumentFile.fromTreeUri(context, dcimRoot);
            DocumentFile fsResource = doc.findFile(FS_RESOURCE);
            if(fsResource != null && fsResource.exists())
                return fsResource;
        }

        try {
            Uri selectedDcimRoot = DocumentFile.fromTreeUri(context, storage.getPriorityDcimRoot()).getUri();
            Uri albumInfoFileUri = DocumentsContract.createDocument(
                    resolver,
                    selectedDcimRoot,
                    "application/octet-stream",
                    FS_RESOURCE);
            return DocumentFile.fromSingleUri(context, albumInfoFileUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        throw new FsSyncResourceCreationError();
    }



    public ArrayList<Album> getSavedAlbums()
    {
        try {

            InputStream istr = resolver.openInputStream(fsResourseFile.getUri());
            ObjectInput ostr = new ObjectInputStream(new ByteArrayInputStream(IOUtils.toByteArray(istr)));
            ArrayList<Album> albums = (ArrayList<Album>) ostr.readObject();

            ostr.close();
            istr.close();

            for (Object album : albums)
                if (!(album instanceof Album))
                    throw new ClassCastException();

            return albums;

        } catch (IOException|ClassNotFoundException e) {
            resetFsResource();
        }

        return new ArrayList<>();
    }


    public void setKnownAlbums(List<Album> albums)
    {
        try{
            OutputStream os = resolver.openOutputStream(fsResourseFile.getUri());
            ObjectOutputStream oos= new ObjectOutputStream(os);
            oos.writeObject(albums);
            oos.close();
            os.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }


    private void resetFsResource()
    {
        setKnownAlbums(new ArrayList<Album>());
    }

}
