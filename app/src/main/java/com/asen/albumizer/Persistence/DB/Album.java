package com.asen.albumizer.Persistence.DB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.Serializable;


@Entity(tableName = "albums", indices = {
        @Index(value = "album_name", unique = true)
})
public class Album implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public transient int id;

    @ColumnInfo(name = "album_name")
    @NonNull
    public String name;

    @ColumnInfo(name = "album_path")
    @NonNull
    public String path;

    @ColumnInfo(name = "hidden")
    public boolean hidden;


    public Album(){

    }


    public Album(@NonNull String albumName, @NonNull String albumPath) {
        this.path = albumPath;
        this.name = albumName;
        this.hidden = false;
    }


    public Album(@NonNull String albumName, @NonNull String albumPath, @NonNull boolean isHidden) {
        this.path = albumPath;
        this.name = albumName;
        this.hidden = isHidden;
    }

    public String getName()
    {
        return name;
    }

    public Uri getPathAsUri()
    {
        return Uri.parse(path);
    }


}