package com.asen.albumizer.Persistence.DB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.asen.albumizer.Persistence.DB.Migrations.Migrate_1_2;

@Database(entities = {Album.class}, version = 2)

public abstract class DB extends RoomDatabase {


    private static DB INSTANCE;
    private static final String DB_NAME = "albums-db";


    public static DB getDatabase(Context context)
    {
        if (INSTANCE == null) {
            synchronized (DB.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room
                            .databaseBuilder(context.getApplicationContext(), DB.class, DB_NAME)
                            .addMigrations(new Migrate_1_2())
                            .build();
                }
            }
        }
        return INSTANCE;
    }



    public abstract AlbumDAO albumDAO();

}
