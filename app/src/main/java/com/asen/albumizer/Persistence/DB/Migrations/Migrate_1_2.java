package com.asen.albumizer.Persistence.DB.Migrations;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

public class Migrate_1_2 extends Migration
{

    public Migrate_1_2()
    {
        super(1,2);
    }

    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
        database.execSQL("ALTER TABLE albums ADD COLUMN hidden INTEGER NOT NULL DEFAULT 0");
    }

}
