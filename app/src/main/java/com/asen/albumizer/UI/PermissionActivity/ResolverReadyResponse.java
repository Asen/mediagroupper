package com.asen.albumizer.UI.PermissionActivity;

import android.content.ContentResolver;

public interface ResolverReadyResponse {

    void onResolverReady(ContentResolver resolver);

}
