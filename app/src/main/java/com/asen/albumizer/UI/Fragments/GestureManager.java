package com.asen.albumizer.UI.Fragments;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.asen.albumizer.Persistence.DB.Album;

public class GestureManager extends ItemTouchHelper.Callback
{

    private ItemRemover remover;


    public GestureManager(ItemRemover remover)
    {
        this.remover = remover;
    }


    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
    {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
    {
        recyclerView.getAdapter().notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return false;
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
    {
        if(viewHolder instanceof AlbumItemRecyclerViewAdapter.AlbumViewHolder){
            Album album = ((AlbumItemRecyclerViewAdapter.AlbumViewHolder) viewHolder).getItem();
            remover.onRemoveAlbum(album);
        }
    }


}
