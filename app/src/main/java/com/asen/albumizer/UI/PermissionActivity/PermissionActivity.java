package com.asen.albumizer.UI.PermissionActivity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.Settings;
import android.support.v4.provider.DocumentFile;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.Toast;

import com.asen.albumizer.Persistence.Storage;
import com.asen.albumizer.R;
import com.asen.albumizer.Logic.Localized;
import com.asen.albumizer.Logic.Toastable;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class PermissionActivity
        extends AppCompatActivity
        implements PermissionRequester, Toastable, Localized
{

    private static final int REQUEST_CODE_DCIM_ACCESS = 0x1737;


    private ViewSkeletonScreen serviceStartLoadingBar;
    private PermissionResponse permissionResponse = null;
    private int expectedDcimCount = 0;
    private int dcimPermissionResponseCounter = 0;
    private boolean hasFailedPermission = false;
    private boolean requestInProcess = false;
    private boolean callbacksCompleted = false;
    private Toast currentToast = null;
    private ExecutorService permissionGrantedQueue = Executors.newSingleThreadExecutor();


    protected abstract ViewGroup getLoadingBarView();


    @Override
    public void getGrantedContentResolver(ResolverReadyResponse onReady)
    {
        new Thread(() -> {

            ContentResolver grantedResolver = getContentResolver();
            Set<Uri> dcimVolumeUris = new Storage(PermissionActivity.this).getDcimUriInfo();

            for (Uri dcimVolume:dcimVolumeUris)
                grantedResolver.takePersistableUriPermission(dcimVolume, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            onReady.onResolverReady(grantedResolver);

        }).start();
    }


    @Override
    public synchronized void requestPermission(PermissionResponse permissionResponse)
    {
        if(requestInProcess) {
            permissionResponse.onFailure();
            return;
        }

        this.permissionResponse = permissionResponse;
        requestInProcess = true;

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .withListener(new MultiplePermissionsListener() {

                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){
                            new Thread(() -> requestDCIMPermissions()).start();
                        } else {
                            permissionResponse.onFailure();
                            requestInProcess = false;
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                        permissionResponse.onFailure();
                        requestInProcess = false;
                    }

                })
                .check();
    }


    @Override
    public void showToast(final int stringId)
    {
        String message = getLocalizedString(stringId);
        showToast(message);
    }


    @Override
    public void showToast(String message)
    {
        runOnUiThread(() -> {

            if(currentToast != null)
                currentToast.cancel();

            currentToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            currentToast.show();
        });
    }


    @Override
    public String getLocalizedString(int id)
    {
        return getString(id);
    }


    @Override
    public String getLocalizedAndFormattedString(int id, Object... args)
    {
        return getString(id, args);
    }


    protected void openPermissionsSettings()
    {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }


    private void requestDCIMPermissions()
    {
        StorageManager sm = (StorageManager)getSystemService(Context.STORAGE_SERVICE);
        List<StorageVolume> attachedVolumes = sm.getStorageVolumes();

        Set<Uri> knownUris = new Storage(this).getDcimUriInfo();
        boolean shouldRequestPermissions = false;

        // check permissions
        if(knownUris.size() == attachedVolumes.size()){
            for(Uri knownUri:knownUris)
                if(!DocumentFile.fromTreeUri(PermissionActivity.this, knownUri).canWrite())
                    shouldRequestPermissions = true;
        } else {
            shouldRequestPermissions = true;
        }

        // request permissions
        if(shouldRequestPermissions){

            runOnUiThread(() -> {
                serviceStartLoadingBar = Skeleton.bind(getLoadingBarView()).load(R.layout.loading).show();
                serviceStartLoadingBar.show();
            });

            expectedDcimCount = attachedVolumes.size();
            dcimPermissionResponseCounter = 0;
            hasFailedPermission = false;
            callbacksCompleted = false;

            for(StorageVolume volume : attachedVolumes){
                Intent intent = volume.createAccessIntent(Environment.DIRECTORY_DCIM);
                startActivityForResult(intent, REQUEST_CODE_DCIM_ACCESS);
            }

        }else {
            permissionResponse.onSuccess();
            requestInProcess = false;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_DCIM_ACCESS) {

            if(serviceStartLoadingBar != null)
                serviceStartLoadingBar.hide();

            permissionGrantedQueue.submit(() -> {

                dcimPermissionResponseCounter++;

                if(resultCode == RESULT_OK){
                    Set<Uri> knownUris = new Storage(PermissionActivity.this).getDcimUriInfo();
                    knownUris.add(data.getData());
                    new Storage(PermissionActivity.this).putDcimUriInfo(knownUris);
                } else {
                    hasFailedPermission = true;
                }

                if(dcimPermissionResponseCounter >= expectedDcimCount)
                    onAllDCIMPermissionResponded();

            });
        }
    }


    private synchronized void onAllDCIMPermissionResponded()
    {
        if(callbacksCompleted)
            return;

        if(hasFailedPermission)
            permissionResponse.onFailure();
        else
            permissionResponse.onSuccess();

        requestInProcess = false;
        callbacksCompleted = true;
    }

}
