package com.asen.albumizer.UI.NavigatorActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.provider.DocumentFile;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asen.albumizer.Logic.BitmapConverter;
import com.asen.albumizer.R;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;

import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class NavigatorAdapter extends RecyclerView.Adapter<NavigatorAdapter.ViewHolder>
{
    private Handler handler = new Handler();
    private Context context;
    private DocumentFile[] images;
    private int picWidth;
    private BitmapConverter converter;
    private ExecutorService asyncBitmapLoader = Executors.newFixedThreadPool(20);


    public NavigatorAdapter(Context context, ContentResolver resolver, DocumentFile[] contents, int picWidthPx) {
        super();

        this.context = context;
        this.images = contents;
        this.picWidth = picWidthPx;
        this.converter = new BitmapConverter(resolver);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.navigator_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        DocumentFile img = images[position];
        holder.load(img);
    }


    @Override
    public int getItemCount()
    {
        return images.length;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private final ImageView imgView;
        private final TextView itemName;
        private Uri uri;
        private SparseArray<Future> loadingQueue = new SparseArray<>();
        private ViewSkeletonScreen serviceStartLoadingBar;


        ViewHolder(View view)
        {
            super(view);
            itemName = view.findViewById(R.id.album_item_name);
            imgView = view.findViewById(R.id.album_item);
            imgView.setOnClickListener(this);
        }


        synchronized void load(DocumentFile img)
        {
            reset();
            showLoading();

            for(int i = 0; i < loadingQueue.size(); i++)
                loadingQueue.valueAt(i).cancel(true);

            loadingQueue.put(img.hashCode(), asyncBitmapLoader.submit(() -> {
                try {

                    Bitmap configuredBmp = converter.convertToBitmap(img, picWidth);

                    handler.post(() -> {
                        if(!loadingQueue.get(img.hashCode()).isCancelled()) {
                            hideLoading();
                            String imgName = FilenameUtils.removeExtension(img.getName());
                            setup(configuredBmp, imgName, img.getUri());
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }));
        }


        void setup(Bitmap image, String title, Uri uri)
        {
            imgView.setImageBitmap(image);
            itemName.setText(title);
            this.uri = uri;
        }


        void reset()
        {
            hideLoading();
            imgView.setImageBitmap(null);
            itemName.setText("");
        }


        void showLoading()
        {
            serviceStartLoadingBar = Skeleton
                    .bind(imgView)
                    .load(R.layout.loading).show();
        }


        void hideLoading()
        {
            if(serviceStartLoadingBar != null){
                serviceStartLoadingBar.hide();
                serviceStartLoadingBar = null;
            }
        }


        @Override
        public void onClick(View v)
        {
            Intent intent = PictureActivity.getCreationIntent(NavigatorAdapter.this.context, uri);
            context.startActivity(intent);
        }


    }

}
