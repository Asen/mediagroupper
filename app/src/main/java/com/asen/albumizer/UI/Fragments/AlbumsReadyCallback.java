package com.asen.albumizer.UI.Fragments;

import com.asen.albumizer.Persistence.DB.Album;

import java.util.List;

public interface AlbumsReadyCallback {
    
    void onAlbumsReady(List<Album> albums);
    
}
