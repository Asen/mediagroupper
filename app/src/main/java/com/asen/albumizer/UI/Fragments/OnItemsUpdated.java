package com.asen.albumizer.UI.Fragments;

public interface OnItemsUpdated {

    void onUpdated();
    void onInProgress();

}
