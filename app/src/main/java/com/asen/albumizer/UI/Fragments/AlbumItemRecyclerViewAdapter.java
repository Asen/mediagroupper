package com.asen.albumizer.UI.Fragments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.R;
import com.asen.albumizer.UI.Fragments.AlbumItemFragment.OnListFragmentInteractionListener;

import java.util.List;


public class AlbumItemRecyclerViewAdapter extends RecyclerView.Adapter<AlbumItemRecyclerViewAdapter.AlbumViewHolder> {

    private final List<Album> mValues;
    private final OnListFragmentInteractionListener mListener;

    public AlbumItemRecyclerViewAdapter(List<Album> items, OnListFragmentInteractionListener listener)
    {
        mValues = items;
        mListener = listener;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);

        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AlbumViewHolder holder, int position)
    {
        Album album = mValues.get(position);
        holder.item = album;

        holder.visibilityImg.setImageResource(album.hidden ?
                R.drawable.invisible_album :
                R.drawable.visible_album);

        holder.contentView.setText(mValues.get(position).name);

        holder.baseView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListFragmentInteraction(holder.item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class AlbumViewHolder extends RecyclerView.ViewHolder
    {
        View baseView;
        ImageView visibilityImg;
        TextView contentView;
        Album item;

        AlbumViewHolder(View view) {
            super(view);
            baseView = view;
            visibilityImg = view.findViewById(R.id.item_visibility);
            contentView = view.findViewById(R.id.content);
        }

        @NonNull
        public Album getItem()
        {
            return item;
        }
    }
}
