package com.asen.albumizer.UI.PermissionActivity;

public interface PermissionResponse
{

    void onSuccess();
    void onFailure();

}
