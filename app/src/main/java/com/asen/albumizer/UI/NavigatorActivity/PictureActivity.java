package com.asen.albumizer.UI.NavigatorActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.support.v4.provider.DocumentFile;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.asen.albumizer.Logic.BitmapConverter;
import com.asen.albumizer.R;
import com.asen.albumizer.UI.PermissionActivity.PermissionActivity;
import com.asen.albumizer.UI.RenamingActivity;

import org.acra.ACRA;
import org.apache.commons.io.FilenameUtils;

import java.io.FileNotFoundException;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PictureActivity extends PermissionActivity
{

    private static final int RENAME_MENU_ITEM = 2;
    private static final int RENAME_PICTURE_REQUEST_CODE = 0x3000;


    @BindView(android.R.id.content)
    ViewGroup rootView;

    @BindView(R.id.picture_view)
    ImageView picView;


    private Uri pictureUri;
    private String pictureFileName;
    private String extension;


    public static Intent getCreationIntent(Context context, Uri documentUri)
    {
        Intent intent = new Intent(context, PictureActivity.class);
        intent.putExtra("pic", documentUri.toString());
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
        ButterKnife.bind(this);

        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pictureUri = Uri.parse(getIntent().getStringExtra("pic"));
        DocumentFile doc = DocumentFile.fromSingleUri(this, pictureUri);

        if(doc != null){
            pictureFileName = doc.getName();
            extension = "."+FilenameUtils.getExtension(pictureFileName);
            displayPicture();
        } else {
            finish();
        }
    }


    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        menu.add(1, RENAME_MENU_ITEM, 1, R.string.ui_activity_picture_rename);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){

            case RENAME_MENU_ITEM:
                renamePicture();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected synchronized void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RENAME_PICTURE_REQUEST_CODE){

            if(resultCode == RESULT_OK){
                getGrantedContentResolver(resolver -> {

                    String newName = RenamingActivity.getNewName(data);
                    DocumentFile doc = DocumentFile.fromSingleUri(this, pictureUri);

                    try {
                        pictureUri = DocumentsContract.renameDocument(resolver, doc.getUri(), newName+extension);
                        pictureFileName = DocumentFile.fromSingleUri(this, pictureUri).getName();
                        displayPicture();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                });
            }

        }
    }


    private void renamePicture()
    {
        Intent intent = RenamingActivity.getCreationIntent(this, pictureFileName);
        startActivityForResult(intent, RENAME_PICTURE_REQUEST_CODE);
    }


    private void displayPicture()
    {
        Display display = getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);

        getGrantedContentResolver(resolver -> {

            DocumentFile doc = DocumentFile.fromSingleUri(this, pictureUri);
            String title = FilenameUtils.removeExtension(doc.getName());

            runOnUiThread(() -> {

                setTitle(title);

                try {
                    Bitmap bmp = new BitmapConverter(resolver).convertToBitmap(doc, point.x);
                    picView.setImageBitmap(bmp);
                } catch (IOException e){
                    ACRA.getErrorReporter().handleException(e);
                }

            });

        });
    }


    @Override
    protected ViewGroup getLoadingBarView()
    {
        return rootView;
    }
}
