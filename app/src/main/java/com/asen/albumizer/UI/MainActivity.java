package com.asen.albumizer.UI;

import android.app.job.JobScheduler;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import com.asen.albumizer.MediaWatcherJob;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.Storage;
import com.asen.albumizer.R;
import com.asen.albumizer.UI.Fragments.AlbumItemFragment;
import com.asen.albumizer.UI.NavigatorActivity.AlbumNavigatorActivity;
import com.asen.albumizer.UI.PermissionActivity.PermissionActivity;
import com.asen.albumizer.UI.PermissionActivity.PermissionResponse;
import com.asen.albumizer.WatcherMonitorService;
import com.kyleduo.switchbutton.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;


public class MainActivity extends PermissionActivity
        implements AlbumItemFragment.OnListFragmentInteractionListener
{

    @BindView(R.id.switch_button)
    SwitchButton switcher;

    @BindView(android.R.id.content)
    ViewGroup rootView;

    @BindView(R.id.toolbar)
    Toolbar toolBar;

    private boolean isInitState = true;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        ButterKnife.bind(this);

        toolBar.setTitle(R.string.app_name);
        setSupportActionBar(toolBar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().
                    beginTransaction().
                    add(R.id.frame, AlbumItemFragment.newInstance()).
                    commit();
        }
    }


    @Override
    protected ViewGroup getLoadingBarView()
    {
        return rootView;
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        initSwitcher();
    }


    private synchronized void initSwitcher()
    {
        runOnUiThread(() -> {
            isInitState = true;
            switcher.setChecked(MediaWatcherJob.isScheduled(MainActivity.this));
            isInitState = false;
        });
    }


    @OnClick(R.id.fab)
    protected void onNewAlbumAdd()
    {
        startActivity(new Intent(this, CreateAlbumActivity.class));
    }


    @OnCheckedChanged(R.id.switch_button)
    protected void onSwitcherChanged()
    {
        if(isInitState)
            return;

        // react on state
        if(switcher.isChecked()) {
            if (MediaWatcherJob.isScheduled(this))
                showToast(R.string.service_already_running);
            else
                startService();
        }
        else {
            if (MediaWatcherJob.isScheduled(this))
                stopService();
            else
                showToast(R.string.service_already_stopped);
        }

        // update permanent state
        new Thread(() -> new Storage(MainActivity.this).putServiceStateInfo(switcher.isChecked())).start();
    }


    private void startService()
    {
        requestPermission(new PermissionResponse() {
            @Override
            public void onSuccess() {
                new Thread(() -> {
                    if (MediaWatcherJob.scheduleJob(MainActivity.this) == JobScheduler.RESULT_SUCCESS) {
                        //WatcherMonitorService.start(MainActivity.this);
                        showToast(R.string.service_active);
                    } else
                        showToast(R.string.service_could_not_start);
                    initSwitcher();
                }).start();
            }

            @Override
            public void onFailure() {
                showToast(R.string.permissions_needed);
                initSwitcher();
                openPermissionsSettings();
            }
        });
    }


    private void stopService()
    {
        new Thread(() -> {
            MediaWatcherJob.cancelJob(MainActivity.this);
            //WatcherMonitorService.stop(MainActivity.this);
            showToast(R.string.service_stopped);
            initSwitcher();
        }).start();
    }


    @Override
    public void onListFragmentInteraction(Album item)
    {
        Intent intent = AlbumNavigatorActivity.getStartIntent(this, item);
        startActivity(intent);
    }

}
