package com.asen.albumizer.UI.PermissionActivity;

public interface PermissionRequester
{

    void requestPermission(PermissionResponse onSuccess);
    void getGrantedContentResolver(ResolverReadyResponse onReady);

}
