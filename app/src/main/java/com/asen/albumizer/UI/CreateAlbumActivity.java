package com.asen.albumizer.UI;

import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.provider.DocumentFile;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;

import com.asen.albumizer.Logic.AlbumManager.AlbumManager;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.DB.DB;
import com.asen.albumizer.Persistence.Storage;
import com.asen.albumizer.R;
import com.asen.albumizer.UI.PermissionActivity.PermissionActivity;
import com.asen.albumizer.UI.PermissionActivity.PermissionResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateAlbumActivity extends PermissionActivity
{

    @BindView(R.id.editText)
    EditText albumNameEditText;

    @BindView(android.R.id.content)
    ViewGroup rootView;

    @BindView(R.id.switch_hidden_gal)
    Switch hiddenFromGalSwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_album);
        ButterKnife.bind(this);
    }


    @Override
    protected ViewGroup getLoadingBarView()
    {
        return rootView;
    }


    @OnClick(R.id.button)
    protected void onCreateAlbum()
    {
        String albumName = albumNameEditText.getText().toString().trim();

        if(albumName.length()==0) {
            showToast(R.string.enter_album_name);
            return;
        }

        final boolean isHiddenFromGallery = hiddenFromGalSwitch.isChecked();

        requestPermission(new PermissionResponse() {
            @Override
            public void onSuccess() {

                new Thread(() -> {

                    Storage storage = new Storage(CreateAlbumActivity.this);
                    Uri albumParent = storage.getPriorityDcimRoot();

                    if(!DocumentFile.fromTreeUri(CreateAlbumActivity.this, albumParent).canWrite()){
                        showToast(R.string.permissions_needed);
                        return;
                    }

                    DocumentFile albumDocRoot = DocumentFile.fromTreeUri(CreateAlbumActivity.this, albumParent);
                    DocumentFile albumDoc = albumDocRoot.findFile(albumName);
                    boolean existingAlbumDir = false;

                    if(albumDoc!=null && albumDoc.exists()){
                        existingAlbumDir = true;
                    }

                    // constraints exception if duplicate
                    Album newAlbum = new Album(albumName, albumParent.toString(), isHiddenFromGallery);
                    long newAlbumId;
                    try {
                        newAlbumId = DB.getDatabase(CreateAlbumActivity.this).albumDAO().insert(newAlbum);
                    } catch (SQLiteConstraintException e){
                        showToast(R.string.album_name_exists);
                        return;
                    }

                    if(!existingAlbumDir)
                        albumDoc = albumDocRoot.createDirectory(albumName);

                    if(albumDoc!=null && albumDoc.exists() && newAlbumId>=0) {
                        syncAlbumsWithFSAsync();
                        showToast(existingAlbumDir ? R.string.album_added : R.string.album_created);
                        finish();
                    } else {
                        showToast(R.string.album_not_created);
                    }

                }).start();

            }

            @Override
            public void onFailure() {
                showToast(R.string.album_create_permissions_needed);
            }
        });
    }


    private void syncAlbumsWithFSAsync()
    {
        getGrantedContentResolver(resolver -> {
            new AlbumManager().synchronizeAlbumsWithFS(this, resolver, null);
        });
    }

}
