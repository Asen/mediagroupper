package com.asen.albumizer.UI.Fragments;

import com.asen.albumizer.Persistence.DB.Album;

public interface ItemRemover {

    void onRemoveAlbum(Album album);

}
