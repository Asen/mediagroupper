package com.asen.albumizer.UI;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.asen.albumizer.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RenamingActivity extends AppCompatActivity
{

    @BindView(R.id.new_name_box)
    EditText newNameBox;

    @BindView(R.id.current_name_label)
    TextView currentNameLabel;


    public static Intent getCreationIntent(Context context, String currentName)
    {
        return new Intent(context, RenamingActivity.class)
                .putExtra("current_name", currentName);
    }


    public static String getNewName(Intent intent)
    {
        return intent.getStringExtra("new_name");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renaming);
        ButterKnife.bind(this);
        setTitle(R.string.ui_rename_activity_title);

        String currentName = getIntent().getStringExtra("current_name");
        currentNameLabel.setText(getResources().getString(R.string.ui_rename_activity_current_name, currentName));
    }


    @OnClick(R.id.rename_button)
    protected void onRename()
    {
        String newName = newNameBox.getText().toString().trim();

        if(newName.length() > 0){
            Intent intent = new Intent();
            intent.putExtra("new_name", newName);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

}
