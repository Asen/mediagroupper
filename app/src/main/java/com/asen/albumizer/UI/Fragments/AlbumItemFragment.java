package com.asen.albumizer.UI.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asen.albumizer.Logic.AlbumManager.AlbumManager;
import com.asen.albumizer.Logic.AlbumManager.StateChangedListener;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.DB.DB;
import com.asen.albumizer.R;
import com.asen.albumizer.Logic.Localized;
import com.asen.albumizer.UI.PermissionActivity.PermissionRequester;
import com.asen.albumizer.UI.PermissionActivity.PermissionResponse;
import com.asen.albumizer.Logic.Toastable;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;

import java.util.ArrayList;


public class AlbumItemFragment extends Fragment
        implements OnRefreshListener, Localized, ItemRemover
{

    public interface OnListFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Album item);
    }

    private Handler uiHandler = new Handler();
    private OnListFragmentInteractionListener mListener;
    private ViewSkeletonScreen serviceStartLoadingBar;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshPuller;
    private PermissionRequester permissionRequester;
    private Toastable toastable;
    private boolean isUpdating = false;


    public static AlbumItemFragment newInstance()
    {
        return new AlbumItemFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(new AlbumItemRecyclerViewAdapter(new ArrayList<Album>(), mListener));
        new ItemTouchHelper(new GestureManager(this)).attachToRecyclerView(recyclerView);

        refreshPuller = view.findViewById(R.id.swipe_refresh);
        refreshPuller.setOnRefreshListener(this);
        refreshPuller.setColorSchemeResources(R.color.colorAccent);

        updateAlbumsList(null);

        return view;
    }


    @Override
    public void onRefresh()
    {
        updateAlbumsList(new OnItemsUpdated() {
            @Override
            public void onUpdated() {
                refreshPuller.setRefreshing(false);
            }
            @Override
            public void onInProgress() {
                refreshPuller.setRefreshing(false);
            }
        });
    }


    @Override
    public void onResume()
    {
        super.onResume();
        updateAlbumsList(null);
    }


    private synchronized void updateAlbumsList(final OnItemsUpdated onUpdated)
    {
        if(isUpdating) {
            if(onUpdated != null)
                onUpdated.onInProgress();
            return;
        }

        if(permissionRequester == null)
            return;

        isUpdating = true;

        uiHandler.post(() -> {
            View view = getView();
            if(view != null)
                serviceStartLoadingBar = Skeleton.bind(view).load(R.layout.loading).show();
        });

        permissionRequester.requestPermission(new PermissionResponse() {
            @Override
            public void onSuccess() {

                getAlbums(albums -> {
                    uiHandler.post(() -> {
                        if(recyclerView != null)
                            recyclerView.setAdapter(new AlbumItemRecyclerViewAdapter(albums, mListener));

                        if(onUpdated != null)
                            onUpdated.onUpdated();

                        if(serviceStartLoadingBar != null){
                            serviceStartLoadingBar.hide();
                            serviceStartLoadingBar = null;
                        }

                        isUpdating = false;
                    });
                });

            }

            @Override
            public void onFailure() {
                uiHandler.post(() ->{
                    if(recyclerView != null)
                        recyclerView.setAdapter(new AlbumItemRecyclerViewAdapter(new ArrayList<>(), mListener));

                    if(serviceStartLoadingBar != null){
                        serviceStartLoadingBar.hide();
                        serviceStartLoadingBar = null;
                    }

                    Context context = getActivity();
                    Toastable toastable = AlbumItemFragment.this.toastable;
                    if(context != null && toastable != null)
                        toastable.showToast(R.string.album_update_permissions_needed);

                    if(onUpdated != null)
                        onUpdated.onUpdated();

                    isUpdating = false;
                });
            }
        });
    }


    private void syncAlbumsWithFSAsync(StateChangedListener listener)
    {
        PermissionRequester localRequester = permissionRequester;
        if(localRequester != null){
            localRequester.getGrantedContentResolver(resolver -> {

                Context context = getActivity();
                if(context == null)
                    return;

                new AlbumManager().synchronizeAlbumsWithFS(context, resolver, listener);
            });
        }
    }


    private void getAlbums(AlbumsReadyCallback callback)
    {
        Context context = getActivity();
        if(context == null)
            return;

        callback.onAlbumsReady(DB.getDatabase(context).albumDAO().getAlbums());

        syncAlbumsWithFSAsync(() -> {
            callback.onAlbumsReady(DB.getDatabase(context).albumDAO().getAlbums());
        });
    }


    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }

        if (context instanceof PermissionRequester) {
            permissionRequester = (PermissionRequester) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement PermissionRequester");
        }

        if (context instanceof Toastable) {
            toastable = (Toastable) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement Toastable");
        }
    }


    @Override
    public void onDetach()
    {
        super.onDetach();

        if(serviceStartLoadingBar != null)
            serviceStartLoadingBar.hide();

        serviceStartLoadingBar = null;
        mListener = null;
        permissionRequester = null;
        toastable = null;
    }


    @Override
    public String getLocalizedString(int id) {
        return getString(id);
    }


    @Override
    public String getLocalizedAndFormattedString(int id, Object... args) {
        return getString(id, args);
    }


    @Override
    public void onRemoveAlbum(Album album)
    {
        new AlertDialog.Builder(getActivity())
                .setTitle(getLocalizedString(R.string.ui_album_fragment_delete_album_dialog_title))
                .setMessage(getLocalizedAndFormattedString(R.string.ui_album_fragment_delete_album_dialog_text, album.getName()))
                .setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
                    ViewSkeletonScreen loader = Skeleton.bind(getView()).load(R.layout.loading).show();
                    deleteAlbum(album, () -> {
                        updateAlbumsList(null);
                        loader.hide();
                        toastable.showToast(R.string.album_removed);
                    });
                })
                .setNegativeButton(R.string.dialog_no, (dialog, which) -> {
                    dialog.cancel();
                    updateAlbumsList(null);
                })
                .setOnDismissListener(dialog  -> updateAlbumsList(null))
                .show();
    }


    private void deleteAlbum(Album album, Runnable onSuccess)
    {
        PermissionRequester localRequester = permissionRequester;
        if(localRequester != null){
            localRequester.getGrantedContentResolver(resolver -> {
                new AlbumManager().deleteAlbumWithFullSync(getActivity(), resolver, album);
                uiHandler.post(onSuccess);
            });
        }
    }

}
