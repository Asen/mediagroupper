package com.asen.albumizer.UI.NavigatorActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.support.v4.provider.DocumentFile;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.asen.albumizer.Errors.AlbumAlreadyExistsError;
import com.asen.albumizer.Logic.AlbumManager.AlbumManager;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.Persistence.DB.DB;
import com.asen.albumizer.R;
import com.asen.albumizer.UI.RenamingActivity;
import com.asen.albumizer.UI.PermissionActivity.PermissionActivity;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumNavigatorActivity extends PermissionActivity
{
    private static final int VISIBILITY_MENU_ITEM = 1;
    private static final int RENAME_MENU_ITEM = 2;
    private static final int DELETE_MENU_ITEM = 3;

    private static final int RENAME_ALBUM_REQUEST_CODE = 0x2000;


    @BindView(R.id.album_contents)
    RecyclerView recyclerView;

    @BindView(R.id.root_view)
    ViewGroup rootView;

    int loadedAlbumId;
    String albumName;
    Uri albumUri;
    boolean isAlbumVisible = false;
    int lastKnownPhotosCount = -1;
    private RecyclerView.LayoutManager recyclerLayoutManager;


    public static Intent getStartIntent(Context ctx, Album album)
    {
        Intent intent = new Intent(ctx, AlbumNavigatorActivity.class);
        intent.putExtra("album_name", album.getName());
        intent.putExtra("album_uri", album.getPathAsUri().toString());
        intent.putExtra("album_visible", !album.hidden);
        intent.putExtra("album_id", album.id);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_navigator);
        ButterKnife.bind(this);

        loadedAlbumId = getIntent().getIntExtra("album_id", -1);
        isAlbumVisible = getIntent().getBooleanExtra("album_visible", false);
        albumName = getIntent().getStringExtra("album_name");
        albumUri = Uri.parse(getIntent().getStringExtra("album_uri"));

        recyclerLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(recyclerLayoutManager);

        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        int visibilityTitle = isAlbumVisible ?
                R.string.ui_activity_navigator_menu_make_album_invisible :
                R.string.ui_activity_navigator_menu_make_album_visible;

        menu.add(1, VISIBILITY_MENU_ITEM, 1, visibilityTitle);
        menu.add(1, RENAME_MENU_ITEM, 1, R.string.ui_activity_navigator_menu_rename_album);
        menu.add(1, DELETE_MENU_ITEM, 1, R.string.ui_activity_navigator_menu_remove_album);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){

            case VISIBILITY_MENU_ITEM:
                switchAlbumStateWithDialog(item, () -> {
                    runOnUiThread(this::invalidateOptionsMenu);
                });
                break;

            case DELETE_MENU_ITEM:
                deleteAlbumWithDialog(item);
                break;

            case RENAME_MENU_ITEM:
                renameAlbum();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected synchronized void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RENAME_ALBUM_REQUEST_CODE){

            if(resultCode == RESULT_OK){
                getGrantedContentResolver(resolver -> {

                    String newName = RenamingActivity.getNewName(data);

                    try {
                        new AlbumManager().renameAlbumWithFullSync(this, resolver, loadedAlbumId, newName);
                        albumName = newName;
                    } catch (AlbumAlreadyExistsError albumAlreadyExistsError) {
                        showToast(getLocalizedString(R.string.album_name_exists));
                        return;
                    }

                    runOnUiThread(() -> {
                        showToast(getLocalizedString(R.string.ui_rename_activity_rename_ok));
                        updateImageList();
                    });

                });
            }

        }
    }

    private void renameAlbum()
    {
        Intent intent = RenamingActivity.getCreationIntent(this, albumName);
        startActivityForResult(intent, RENAME_ALBUM_REQUEST_CODE);
    }


    private void deleteAlbumWithDialog(MenuItem item)
    {
        new AlertDialog.Builder(this)
                .setTitle(getLocalizedString(R.string.ui_album_fragment_delete_album_dialog_title))
                .setMessage(getLocalizedAndFormattedString(R.string.ui_album_fragment_delete_album_dialog_text, albumName))
                .setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
                    item.setEnabled(false);
                    Skeleton.bind(getLoadingBarView()).load(R.layout.loading).show();
                    deleteAlbum(() -> {
                        showToast(R.string.album_removed);
                        finish();
                    });
                })
                .setNegativeButton(R.string.dialog_no, (dialog, which) -> {
                    dialog.cancel();
                })
                .show();
    }


    private void deleteAlbum(Runnable onSuccess)
    {
        getGrantedContentResolver(resolver -> {
            Album album = DB.getDatabase(this).albumDAO().getById(loadedAlbumId);
            new AlbumManager().deleteAlbumWithFullSync(this, resolver, album);
            onSuccess.run();
        });
    }


    private void switchAlbumStateWithDialog(MenuItem item, Runnable onStateSwitched)
    {
        new AlertDialog.Builder(this)
                .setTitle(getLocalizedString(R.string.switch_album_state_title))
                .setMessage(getLocalizedString(R.string.switch_album_state_message))
                .setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
                    item.setEnabled(false);
                    ViewSkeletonScreen loader = Skeleton.bind(getLoadingBarView()).load(R.layout.loading).show();
                    switchAlbumStateAsync(() -> {
                        runOnUiThread(loader::hide);
                        runOnUiThread(() -> item.setEnabled(true));
                        onStateSwitched.run();
                    });
                })
                .setNegativeButton(R.string.dialog_no, (dialog, which) -> {
                    dialog.cancel();
                })
                .show();
    }


    private void switchAlbumStateAsync(Runnable onComplete)
    {
        getGrantedContentResolver(resolver -> {

            DocumentFile album = DocumentFile.fromTreeUri(this, albumUri).findFile(albumName);
            ExecutorService executorService = Executors.newFixedThreadPool(10);

            for(DocumentFile photo : album.listFiles()){
                executorService.submit(() -> {
                    try {
                        String name = FilenameUtils.removeExtension(photo.getName()) +
                                (isAlbumVisible ? ".hid" : ".jpg");
                        Uri newDoc = DocumentsContract.createDocument(resolver, album.getUri(),
                                isAlbumVisible ? "application/octet-stream" : "image/jpeg", name);
                        InputStream istr = resolver.openInputStream(photo.getUri());
                        OutputStream ostr = resolver.openOutputStream(newDoc);
                        IOUtils.copy(istr, ostr);
                        DocumentsContract.deleteDocument(resolver, photo.getUri());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                });
            }

            try {
                executorService.shutdown();
                executorService.awaitTermination(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(isAlbumVisible)
                DB.getDatabase(this).albumDAO().setAlbumInvisible(loadedAlbumId);
            else
                DB.getDatabase(this).albumDAO().setAlbumVisible(loadedAlbumId);

            isAlbumVisible = !isAlbumVisible;
            onComplete.run();

        });

    }


    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        DocumentFile album = DocumentFile.fromTreeUri(this, albumUri).findFile(albumName);
        if(album == null || !album.exists()) {
            finish();
            return;
        }

        updateImageListIfNeeded();
    }


    private void updateImageListIfNeeded()
    {
        DocumentFile album = DocumentFile.fromTreeUri(this, albumUri).findFile(albumName);
        int photoCount = album.listFiles().length;

        //if(photoCount != lastKnownPhotosCount) {
            lastKnownPhotosCount = photoCount;
            updateImageList();
       // }
    }


    private void updateImageList()
    {
        getGrantedContentResolver(resolver -> {

            DocumentFile album = DocumentFile.fromTreeUri(this, albumUri).findFile(albumName);

            if(album == null || !album.exists())
                return;

            Display display = getWindowManager().getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);

            runOnUiThread(() -> {
                Parcelable recyclerViewState = recyclerLayoutManager.onSaveInstanceState();
                setTitle(albumName + " (" + lastKnownPhotosCount + ")");
                recyclerView.setAdapter(new NavigatorAdapter(this, resolver, album.listFiles(), (int) (point.x / 2.5)));
                recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            });

        });
    }


    @Override
    protected ViewGroup getLoadingBarView() {
        return rootView;
    }
}
