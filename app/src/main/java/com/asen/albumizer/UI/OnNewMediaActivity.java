package com.asen.albumizer.UI;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.support.v4.provider.DocumentFile;
import android.view.ViewGroup;
import android.widget.EditText;

import com.asen.albumizer.Errors.MediaNotFoundError;
import com.asen.albumizer.Logic.MediaInfo;
import com.asen.albumizer.Persistence.DB.Album;
import com.asen.albumizer.R;
import com.asen.albumizer.UI.Fragments.AlbumItemFragment;
import com.asen.albumizer.UI.PermissionActivity.PermissionActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.stfalcon.frescoimageviewer.ImageViewer;

import org.acra.ACRA;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnNewMediaActivity extends PermissionActivity
        implements AlbumItemFragment.OnListFragmentInteractionListener
{

    @BindView(R.id.photo_title)
    EditText photoTitle;

    @BindView(R.id.changed_pic)
    SimpleDraweeView changedPicture;

    @BindView(android.R.id.content)
    ViewGroup rootView;

    Uri objectiveMediaUri;


    public static Intent getCreationIntent(Context context, Uri uri, int notificationId)
    {
        return new Intent(context, OnNewMediaActivity.class)
                .putExtra("changed_uri", uri.toString())
                .putExtra("notification_id", notificationId);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setFinishOnTouchOutside(false);

        Fresco.initialize(this);
        setContentView(R.layout.activity_on_media);
        ButterKnife.bind(this);

        String changedUri = getIntent().getStringExtra("changed_uri");
        if(changedUri == null) {
            showToast(R.string.new_media_data_not_passed);
            shutDownActivity();
        } else {

            objectiveMediaUri = Uri.parse(changedUri);
            closeIfMediaNotExists();
            changedPicture.setImageURI(objectiveMediaUri);

            if (savedInstanceState == null) {
                getSupportFragmentManager().
                        beginTransaction().
                        add(R.id.frame, AlbumItemFragment.newInstance()).
                        commit();
            }
        }
    }


    private void closeIfMediaNotExists()
    {
        try {

            MediaInfo mInfo = new MediaInfo(getContentResolver(), objectiveMediaUri);
            if(!mInfo.isExists())
                shutDownActivity();

        } catch (MediaNotFoundError err){
            shutDownActivity();
        }
    }


    private void shutDownActivity()
    {
        cancelNotification();
        finish();
    }


    private void cancelNotification()
    {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = getIntent().getIntExtra("notification_id", 0);
        mNotificationManager.cancel(notificationId);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected ViewGroup getLoadingBarView()
    {
        return rootView;
    }


    @OnClick(R.id.leave_media)
    protected void onLeavePicture()
    {
        shutDownActivity();
    }


    @OnClick(R.id.changed_pic)
    protected void onPreviewClick()
    {
        new ImageViewer.Builder(this, Arrays.asList(new Uri[]{objectiveMediaUri}))
                .setStartPosition(0)
                .show();
    }


    @OnClick(R.id.del_media)
    protected void onDeletePicture()
    {
        new AlertDialog.Builder(this)
                .setTitle(R.string.new_media_picture_delete_alert_title)
                .setMessage(R.string.new_media_picture_delete_alert_message)
                .setPositiveButton(R.string.dialog_yes, (dialog, which) -> deletePicture())
                .setNegativeButton(R.string.dialog_no, (dialog, which) -> dialog.cancel())
                .show();
    }


    @OnClick(R.id.fab)
    protected void onAddAlbum()
    {
        startActivity(new Intent(this, CreateAlbumActivity.class));
    }


    @Override
    public void onListFragmentInteraction(final Album item)
    {
        performStorageWritableOperation(() -> movePictureToAlbum(item, photoTitle.getText().toString()));
    }


    private void performStorageWritableOperation(final Runnable task)
    {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        task.run();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        throw new SecurityException();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                    }
                })
                .check();
    }


    private void deletePicture()
    {
        getGrantedContentResolver(resolver -> {
            try{

                MediaInfo mediaInfo = new MediaInfo(resolver, objectiveMediaUri);
                Uri docUri = mediaInfo.getDocumentUri(OnNewMediaActivity.this);
                DocumentsContract.deleteDocument(resolver,  docUri);
                resolver.delete(objectiveMediaUri, null, null);

                showToast(R.string.new_media_picture_deleted);
                shutDownActivity();

            } catch (MediaNotFoundError|FileNotFoundException e) {
                showToast(R.string.new_media_media_not_found);
                shutDownActivity();
            }
        });
    }


    private void movePictureToAlbum(final Album album, final String title)
    {
        getGrantedContentResolver(resolver -> {

            Uri documentUriOfObjective;
            MediaInfo mediaInfo;

            try {
                mediaInfo = new MediaInfo(resolver, objectiveMediaUri);
                documentUriOfObjective = mediaInfo.getDocumentUri(this);
            } catch (MediaNotFoundError e){
                showToast(R.string.new_media_media_not_found);
                return;
            }

            DocumentFile albumPath = DocumentFile.fromTreeUri(this, album.getPathAsUri());
            Uri albumUri = albumPath.findFile(album.getName()).getUri();

            boolean isTitleEmpty = title==null || title.trim().length()==0;
            String picName = isTitleEmpty ? FilenameUtils.removeExtension(mediaInfo.getName()) : title;
            String picExt = album.hidden ? ".hid" : ".jpg";
            String picMime = album.hidden ? "application/octet-stream" : "image/jpeg";

            Uri targetUri;

            try {
                targetUri = DocumentsContract.createDocument(resolver, albumUri, picMime, picName+picExt);
            } catch (FileNotFoundException e) {
                showToast(R.string.new_media_could_not_move_picture);
                return;
            }

            boolean isTargetWritable = DocumentFile.fromSingleUri(this, targetUri).canWrite();
            boolean isSourceReadable = DocumentFile.fromSingleUri(this, documentUriOfObjective).canRead();
            if(!isTargetWritable|| !isSourceReadable){
                showPermissionAlert();
                return;
            }

            try {
                OutputStream oStr = resolver.openOutputStream(targetUri);
                InputStream iStr = resolver.openInputStream(documentUriOfObjective);
                IOUtils.copy(iStr, oStr);
                iStr.close();
                oStr.close();

                DocumentsContract.deleteDocument(resolver, documentUriOfObjective);
                resolver.delete(objectiveMediaUri, null, null);
                showToast(getLocalizedAndFormattedString(R.string.new_media_picture_moved, album.getName(), picName));
                shutDownActivity();
            } catch (IOException e){
                ACRA.getErrorReporter().handleException(e);
            }

        });
    }


    void showPermissionAlert()
    {
        showToast(R.string.permissions_needed);
        ACRA.getErrorReporter().handleException(new SecurityException());
    }


}
